from __future__ import print_function
import random

def main():
    while True:
        for i in range(16):
            for c in "SHAME":
                num = random.randint(1, 7)
                color = "\033[" + str(30 + num) + ";40m"
                print(color + c, end="")
        print()


if __name__ == "__main__":
    main()
